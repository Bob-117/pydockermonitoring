# Data integration

DIK : Data => Information => Knowledge

Cours : https://gitlab.com/Bob-117/epsi_rennes_ingenierie1_cours


## Monitoring d'un SI de containers Docker

### 0) Entités :

- Postgres (`docker`)
- Api (`docker`)
- Client (`docker`)
- Keycloak (`./keycloak/keycloak.sh`)
- *Yara et gestion de fichiers ?*

### 1) Données :

- py logging
- docker logs
- psql select * from mytable where inserted last hour

### 2) Information :

- Evenement sur les applications
- Statut des containers
- Activité de la base de données

### 3) Connaissance :

#### - Utilisation actuelle du SI

- Traffic api
- Nb users

#### - Securité du SI

- Error log
- Error keycloak
- nb process in container

### 4) Décision :

- Levée d'incident si utilisation intenisve (log error, container memory, table effacée)

### Points clés :

- [x] Valoriser la donnée


- [x] Temps réel


```shell
python etl/docker_cpu.py
```

![](readme/3_docker_plot_2.png)

```shell
gcc etl/watch_docker_with_c.c -o etl/watch_docker
./etl/watch_docker
```

```sh
2023-12-18 10:04:41 : gibson-keycloak - 0.52%
2023-12-18 10:04:43 : gibson-keycloak - 0.60%
2023-12-18 10:04:46 : gibson-keycloak - 0.54%
2023-12-18 10:04:48 : gibson-keycloak - 0.60%
2023-12-18 10:04:51 : gibson-keycloak - 0.47%
```


- [ ] Nettoyer la donnée


- [ ] Stocker


