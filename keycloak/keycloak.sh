#!/bin/bash
########################################################################################################################
# Global Variables
########################################################################################################################

# KEYCLOAK
export KEYCLOAK_ADMIN=admin
export KEYCLOAK_ADMIN_PASSWORD=admin
export KEYCLOAK_PORT="8080:8080"
export KEYCLOAK_IMAGE="quay.io/keycloak/keycloak:20.0.1"
export KEYCLOAK_CONTAINER_NAME="gibson-keycloak"
export KEYCLOAK_MODE="start-dev"
export SOURCE_VOLUME
SOURCE_VOLUME=$PWD
export DESTINATION_VOLUME=tmp
export KEYCLOAK_URL=http://localhost:8080

# REALM
export GRANT_TYPE=password
export CLIENT_ID=admin-cli
SCRIPT_DIR="$(cd "$(dirname "$0")" && pwd)"
export REALM_FILE_PATH="$SCRIPT_DIR/gibson_realm.json"
export REALM_NAME=nasa-realm

# TIMEOUT
export TIMEOUT=180


########################################################################################################################
# Functions definition
########################################################################################################################

function startKeycloak() {
    echo "************************************"
    echo "** DOCKER RUN GIBSON KEYCLOAK"
    echo "************************************"
    docker run --rm -d --memory "512m" \
               --name $KEYCLOAK_CONTAINER_NAME \
               -e KEYCLOAK_ADMIN=$KEYCLOAK_ADMIN \
               -e KEYCLOAK_ADMIN_PASSWORD=$KEYCLOAK_ADMIN_PASSWORD \
               -p $KEYCLOAK_PORT $KEYCLOAK_IMAGE $KEYCLOAK_MODE \

    # -v "$SOURCE_VOLUME":$DESTINATION_VOLUME
}

function exit_if_container_not_found_or_stopped() {
    if ! docker ps -a --filter "name=$KEYCLOAK_CONTAINER_NAME" --format '{{.ID}}' 2>/dev/null || 
    [[ $(docker inspect -f '{{.State.Status}}' "$KEYCLOAK_CONTAINER_NAME" 2>/dev/null) != "running" ]]; then
        echo "$(date +"%Y-%m-%d %H:%M:%S"): Container not found or stopped/deleted. Exiting."
        exit 1
    fi
}



function wait_keycloak_is_up() {
    echo "************************************"
    echo "** WAITING FOR KEYCLOAK INSTANCE"
    echo "************************************"

    timeout=$TIMEOUT
    start_time=$(date +%s)

    while true; do

        # Exit if container is suddenly removed
        exit_if_container_not_found_or_stopped


        # Handle timeout
        current_time=$(date +%s)
        current_time_readable=$(date +"%Y-%m-%d %H:%M:%S")
        delta=$((current_time - start_time))

        if [ $delta -ge $timeout ]; then
            echo "$current_time_readable: Container not ready after $timeout s. Exiting."
            exit 1
        fi


        # Watch Keycloak Container and checking for the log $log_message
        container_status=$(docker inspect -f '{{.State.Status}}' "$KEYCLOAK_CONTAINER_NAME")
        wanted_log_message="Running the server in development mode"
        container_last_log=$(docker logs --tail 1 $KEYCLOAK_CONTAINER_NAME)

        if [[ "$container_last_log" == *"$wanted_log_message"* ]]; then
            sleep 5
            configureKeycloakRealm
            break
        else
            echo "$current_time_readable : Container is < $container_status > but not ready for initial realm creation."
            echo "Last log : $container_last_log"
            echo "Checking again in 5 seconds ($delta / $timeout s max)..."
            echo "------------------------------------------------------------------------"
            sleep 10
        fi

    done
}

function configureKeycloakRealm() {
    echo "************************************"
    echo "** Configure Keycloak realm"
    echo "************************************"

    access_token=$( curl -d "client_id=$CLIENT_ID" \
                         -d "username=$KEYCLOAK_ADMIN" \
                         -d "password=$KEYCLOAK_ADMIN_PASSWORD" \
                         -d "grant_type=$GRANT_TYPE" "$KEYCLOAK_URL/realms/master/protocol/openid-connect/token" | \
                         sed -n 's|.*"access_token":"\([^"]*\)".*|\1|p' \
                  )

    # echo "Access token : $access_token"

    if [ "$access_token" = "" ]; then
        echo "------------------------------------------------------------------------"
        echo "Error : "
        echo "======"
        echo ""
        echo "It seems there is a problem to get the Keycloak access token: ($access_token)"
        echo "The script exits here!"
        echo ""
        echo "------------------------------------------------------------------------"
        exit 1
    fi

    # Create Keycloak Realm
    echo "------------------------------------------------------------------------"
    echo "Create the realm in Keycloak"
    echo "------------------------------------------------------------------------"
    echo ""

    result=$(curl -d @"$REALM_FILE_PATH"\
                  -H "Content-Type: application/json" \
                  -H "Authorization: bearer $access_token" "$KEYCLOAK_URL/admin/realms"
            )

    if [ "$result" = "" ]; then
        echo "------------------------------------------------------------------------"
        echo "The realm is created. "
        echo "Open following link in your browser:"
        echo "$KEYCLOAK_URL/admin/master/console/#/$REALM_NAME"
        echo "------------------------------------------------------------------------"
        echo
        docker logs -f $KEYCLOAK_CONTAINER_NAME
    else
        echo "------------------------------------------------------------------------"
        echo "Error:"
        echo "======"
        echo "It seems there is a problem with the realm creation: $result"
        echo "The script exits here!"
        echo ""
        exit 1
    fi
}

########################################################################################################################
# Execution
########################################################################################################################

startKeycloak
wait_keycloak_is_up
