import keycloak
import requests
from fastapi import HTTPException
from keycloak import KeycloakOpenID

COUNT = 0

########################################################################################################################
# Keycloak
########################################################################################################################
keycloak_openid = KeycloakOpenID(
    server_url="http://localhost:8080/",
    client_id="gibson-1",
    realm_name="gibson-realm",
    client_secret_key="gibson"
)


class Response:
    def __init__(self, status_code, msg):
        self.status_code = status_code
        self.msg = msg

    def __str__(self):
        return f'{self.status_code} - {self.msg}'


class Error(Exception):
    status_code: str
    detail: str

    def __init__(self, status_code, detail):
        self.status_code = status_code
        self.detail = detail


########################################################################################################################
# Core methods
########################################################################################################################
def __authenticate_user(username: str, password: str):
    try:
        token = keycloak_openid.token(username, password, grant_type="password")
        return token
    except keycloak.exceptions.KeycloakAuthenticationError:
        raise Error(status_code=401, detail="Invalid credentials")
    except keycloak.exceptions.KeycloakConnectionError:
        raise Error(status_code=503, detail="KeycloakConnectionError")
    except keycloak.exceptions.KeycloakPostError:
        raise Error(status_code=503, detail="KeycloakPostError")


def authenticate_user(username: str, password: str):
    try:
        t = __authenticate_user(username=username, password=password)
        r = Response(status_code=200, msg=t)
    except Error as e:
        r = Response(status_code=e.status_code, msg=e.detail)
    return r


def access_protected_route(token):
    headers = {
        "Authorization": f"Bearer {token}"
    }

    response = requests.get("http://localhost:8017/protected", headers=headers)

    if response.status_code == 200:
        return response.json()
    else:
        # print(f'Error   : {response.status_code} : {response.json()}')
        raise HTTPException(status_code=response.status_code, detail="Failed to access the protected route")


def ask_user_creds():
    global COUNT
    COUNT += 1
    u = input('User : ')
    p = input('Pass : ')
    return u, p


def ask_user_action():
    _a = input('Action : ').upper()
    return _a


def _do(_a):
    print(f'action {_a}')
    if _a == 'E':
        exit(117)


def process():
    r = authenticate_user(*ask_user_creds())
    print(r)
    while r.status_code != 200:
        r = authenticate_user(*ask_user_creds())
        if r.status_code != 200:
            print(r)
    print(f'Connected : {r}')

    while True:
        _a = ask_user_action()
        if _a not in ['C', 'R', 'U', 'D', 'E']:
            _a = ask_user_action()
        else:
            _do(_a)


if __name__ == '__main__':
    process()
