from fastapi import FastAPI, Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from keycloak import KeycloakOpenID, KeycloakAuthenticationError
from starlette.responses import JSONResponse

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

keycloak_openid = KeycloakOpenID(
    server_url="http://localhost:8080/",
    client_id="nasa-client",
    realm_name="nasa-realm",
    client_secret_key="nasa_secret"
)


@app.get("/protected")
async def protected_route(token: str = Depends(oauth2_scheme)):
    protected_data = "PYTHON"
    try:
        token_info = keycloak_openid.userinfo(token)
        print(token_info)
    except KeycloakAuthenticationError:
        return JSONResponse(content={"message": "This is a protected route"}, status_code=403)
    return JSONResponse(content={"message": f"protected data : {protected_data}"}, status_code=200)


@app.get('/')
def gibson():
    return 'gibson'
