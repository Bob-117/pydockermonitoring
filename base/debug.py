import datetime
import json
import subprocess

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


def docker_stats_dict(container):
    try:
        r = subprocess.run(
            ['docker', 'stats', f'{container}', '--format', "{{ json . }}", '--no-stream'], capture_output=True
        )
        print('R : ', r)
        # _d = r.stdout.decode().replace('\n', '')
        # print(_d)
        return json.loads(r)
    except Exception as e:
        raise e


def watch_two_containers(container1, container2, _scroll_value=50):
    fig = plt.figure(figsize=(6, 3))
    ax1 = fig.add_subplot(111)

    x = [0]
    cpu_perc_container1 = [0]
    cpu_perc_container2 = [0]

    date = [datetime.datetime.now()]

    ln1, = ax1.plot(x, cpu_perc_container1, label=container1)
    ln2, = ax1.plot(x, cpu_perc_container2, label=container2)

    def update(frame):
        try:
            new_dict1 = docker_stats_dict(container1)
            new_value1 = float(new_dict1.get('CPUPerc').replace('%', ''))
        except Exception as e:
            new_value1 = 0

        try:
            new_dict2 = docker_stats_dict(container2)
            new_value2 = float(new_dict2.get('CPUPerc').replace('%', ''))
        except Exception as e:
            new_value2 = 0

        cpu_perc_container1.append(new_value1)
        cpu_perc_container2.append(new_value2)
        x.append(x[-1] + 1)

        if len(x) > _scroll_value or len(cpu_perc_container1) > _scroll_value:
            x.pop(0)
            cpu_perc_container1.pop(0)
            cpu_perc_container2.pop(0)

        ln1.set_data(x, cpu_perc_container1)
        ln2.set_data(x, cpu_perc_container2)
        fig.gca().relim()
        fig.gca().autoscale_view()

        return ln1, ln2

    animation = FuncAnimation(fig, update, interval=10)
    plt.legend()
    plt.show()


def scrollable_monitoring_1(containers, _scroll_value=50):
    fig = plt.figure(figsize=(6, 3))
    ax1 = fig.add_subplot(111)

    x = [0]
    cpu_perc = [[0] for _ in range(len(containers))]

    date = [datetime.datetime.now()]

    ln = [ax1.plot(x, cpu_perc[i], label=containers[i])[0] for i in range(len(containers))]

    def update(frame):
        for i, container in enumerate(containers):
            try:
                new_dict = docker_stats_dict(container)
                new_value = float(new_dict.get('CPUPerc').replace('%', ''))
            except Exception as e:
                new_value = 0

            cpu_perc[i].append(new_value)

            if len(cpu_perc[i]) > _scroll_value:
                cpu_perc[i].pop(0)

            ln[i].set_data(x[-len(cpu_perc[i]):], cpu_perc[i])

        x.append(x[-1] + 1)

        ln[0].axes.relim()
        ln[0].axes.autoscale_view()

        return ln

    animation = FuncAnimation(fig, update, interval=10)
    plt.legend()
    plt.show()


def docker_stats_dict_1(containers):
    a = datetime.datetime.now()
    try:
        r = subprocess.run(
            ['docker', 'stats', '--no-stream', '--format', '{{json .}}'] + containers,
        )
        # output = r.stdout.decode().split('\n')
        output = r.stdout
        stats = [json.loads(line) for line in output if line]
        b = datetime.datetime.now()
        print(b - a)
        print( {stat['Name']: float(stat['CPUPerc'].replace('%', '')) for stat in stats})
    except Exception as e:
        print( e)


if __name__ == '__main__':
    # watch_two_containers(_scroll_value=100, container1='gibson-api', container2='gibson-keycloak')
    _ = ['gibson-api', 'gibson-client', 'gibson-keycloak']
    _ = ['gibson-keycloak']
    docker_stats_dict_1(_)