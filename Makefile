kc:
	./keycloak/keycloak.sh

build_api:
	docker build -f api/Dockerfile -t gibson-api:1.0 api

run_api: build_api
	docker run --rm --name gibson-api -p 8001:8001 gibson-api:1.0


build_client:
	docker build -f client/Dockerfile -t gibson-client:1.0 client


run_client: build_client
	docker run --rm --name gibson-client -it gibson-client:1.0

build_c_watcher:
	gcc etl/watch_docker_with_c.c -o etl/watch_docker

run_c_watcher: build_c_watcher
	./etl/watch_docker