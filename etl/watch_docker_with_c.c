#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define CONTAINER_NAME "gibson-keycloak"

int main() {
    char *container_name = CONTAINER_NAME;
    char *cpu_perc = (char *)malloc(10);
    char *timestamp = (char *)malloc(20);
    char *values[3];

    while (1) {
        FILE *fp;
        char command[256];
        sprintf(command, "docker stats --no-stream --format '{{.CPUPerc}}' %s", container_name);
        fp = popen(command, "r");
        if (fp == NULL) {
            fprintf(stderr, "Failed to run command: %s", command);
            exit(1);
        }
        if (fgets(cpu_perc, 10, fp) == NULL) {
            fprintf(stderr, "Failed to read output of command: %s", command);
            exit(1);
        }
        pclose(fp);

        time_t t = time(NULL);
        struct tm *tm = localtime(&t);
        strftime(timestamp, 20, "%Y-%m-%d %H:%M:%S", tm);

        values[0] = container_name;
        values[1] = cpu_perc;
        values[2] = timestamp;

        printf("%s : %s - %s", timestamp, container_name, cpu_perc);

        sleep(1);
    }

    free(cpu_perc);
    free(timestamp);

    return 0;
}
