import datetime
import json
import subprocess

import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation


########################################################################################################################
def docker_stats_dict(container):
    a = datetime.datetime.now()
    try:
        r = subprocess.run(
            ['docker', 'stats', f'{container}', '--format', "{{ json . }}", '--no-stream'], capture_output=True
        )
        _d = r.stdout.decode().replace('\n', '')
        # print(_d)
        b = datetime.datetime.now()
        print(b - a)
        return json.loads(_d)
    except Exception as e:
        raise e


########################################################################################################################
def watch_container(container, _scroll_value=50):
    # Fig
    fig = plt.figure(figsize=(6, 3))
    ax = fig.add_subplot(111)

    # Init data
    x = [0]
    cpu_perc_container1 = [0]

    date = [datetime.datetime.now()]

    # Init plot
    ln1, = ax.plot(x, cpu_perc_container1, label=container)

    def update(frame):

        # Get new values
        try:
            new_dict1 = docker_stats_dict(container)
            new_value1 = float(new_dict1.get('CPUPerc').replace('%', ''))
        except Exception as e:
            new_value1 = 0

        # Set new values
        cpu_perc_container1.append(new_value1)
        x.append(x[-1] + 1)

        # Remove first value for auto scroll
        if len(x) > _scroll_value or len(cpu_perc_container1) > _scroll_value:
            x.pop(0)
            cpu_perc_container1.pop(0)

        # Plot new values
        ln1.set_data(x, cpu_perc_container1)

        # Update fig
        fig.gca().relim()
        fig.gca().autoscale_view()

        return ln1

    animation = FuncAnimation(fig, update, interval=10)
    plt.legend()
    plt.show()


########################################################################################################################
def watch_two_containers(container1, container2, _scroll_value=50):
    # Fig
    fig = plt.figure(figsize=(6, 3))
    ax1 = fig.add_subplot(111)

    # Init data
    x = [0]
    cpu_perc_container1 = [0]
    cpu_perc_container2 = [0]

    date = [datetime.datetime.now()]

    # Init plot
    ln1, = ax1.plot(x, cpu_perc_container1, label=container1)
    ln2, = ax1.plot(x, cpu_perc_container2, label=container2)

    def update(frame):

        # Get new values
        try:
            new_dict1 = docker_stats_dict(container1)
            new_value1 = float(new_dict1.get('CPUPerc').replace('%', ''))
        except Exception as e:
            new_value1 = 0

        try:
            new_dict2 = docker_stats_dict(container2)
            new_value2 = float(new_dict2.get('CPUPerc').replace('%', ''))
        except Exception as e:
            new_value2 = 0

        # Set new values
        cpu_perc_container1.append(new_value1)
        cpu_perc_container2.append(new_value2)
        x.append(x[-1] + 1)

        # Remove first value for auto scroll
        if len(x) > _scroll_value or len(cpu_perc_container1) > _scroll_value:
            x.pop(0)
            cpu_perc_container1.pop(0)
            cpu_perc_container2.pop(0)

        # Plot new values
        ln1.set_data(x, cpu_perc_container1)
        ln2.set_data(x, cpu_perc_container2)

        # Update fig
        fig.gca().relim()
        fig.gca().autoscale_view()

        return ln1, ln2

    animation = FuncAnimation(fig, update, interval=10)
    plt.legend()
    plt.show()


########################################################################################################################

def watch_more_containers(containers, _scroll_value=50):
    fig, axs = plt.subplots(len(containers), 1, figsize=(6, 3 * len(containers)))

    x = [0]
    cpu_perc_containers = [[0] for _ in range(len(containers))]

    date = [datetime.datetime.now()]

    lns = []
    for i, container in enumerate(containers):
        ln, = axs[i].plot(x, cpu_perc_containers[i])
        axs[i].set_title(container)
        lns.append(ln)

        plt.legend()

    def update(frame):
        for _i, _container in enumerate(containers):
            try:
                new_dict = docker_stats_dict(_container)
                new_value = float(new_dict.get('CPUPerc').replace('%', ''))
            except Exception as e:
                new_value = 0
            cpu_perc_containers[_i].append(new_value)

        x.append(x[-1] + 1)

        if len(x) > _scroll_value or len(cpu_perc_containers[0]) > _scroll_value:
            x.pop(0)
            for _l in cpu_perc_containers:
                _l.pop(0)

        for _j, (_ln, c_perc) in enumerate(zip(lns, cpu_perc_containers)):
            _ln.set_data(x, c_perc)
            axs[_j].relim()
            axs[_j].autoscale_view()

        return lns

    animation = FuncAnimation(fig, update, interval=10)

    fig.tight_layout(pad=5.0)
    plt.show()


########################################################################################################################

def monitoring_process(_c):
    if type(_c) is list:
        if len(_c) == 1:
            watch_container(_c[0])
        elif len(_c) == 2:
            watch_two_containers(_c[0], _c[1])
        else:
            watch_more_containers(_c)
    elif type(_c) is str:
        watch_container(_c)


if __name__ == '__main__':
    _ = ['gibson-api', 'gibson-keycloak']
    _ = ['gibson-keycloak']
    _ = ['gibson-api', 'gibson-client', 'gibson-keycloak']
    monitoring_process(_c=_)
